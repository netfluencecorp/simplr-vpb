# Visual Page Builder
Netfluence's Visual Page builder is a collection of lego-like modules for creating HubSpot pages with ease.

# Installation 
1. Install Node.js, a Javascript runtime environment that enables the local tools. Versions 8.9.1 or higher of Node are supported, but we recommend the long-term support (LTS) version.
2. Clone this repository
3. Install Node Dependencies
    `npm install`

# Working with the code

1. Run `npx hs auth oauth2` in the command line. This will begin a series of command line prompts.
Enter your CMS Portal id or your Dev Portal id.
2. Enter your client id and client secret from your private developer app. This will open a window in your default browser requesting authorization. Always use our official [VPB HubSpot App](https://app.hubspot.com/developer/5145327/application/205803)
3. Select your CMS portal on the authorization page.
Request for Integrations Permissions: your private app will request permission to access your account data. Click *"Grant Access"*.

    If successful, you should see *"Authorization Succeeded"*, and your `hubspot.config.yml` file will be updated.


4. Modify your `hubspot.config.yml` to include `id` of the css or js file in hubspot. First of all if you are selecting a existing file within design manager navigate to this file. Otherwise create a new file in your desired path. In the image below you can find the ID for the given file at the end of its url within file manager. e.g 6023636249.

```
development:
  css:
    useLocal: true
    overWriteLocal: false
    id: 21650609312
  js:
    useLocal: true
    overWriteLocal: false
    id: 8575626450
```



# Uploading local modules to a portal

Using the upload command, run `npx hs upload --portal=<name> <src> <dest>` to upload the project in your local environment to your HubSpot account. This will copy the contents of the `<src>` and upload them to your HubSpot account in the <dest>. If you're creating a new project in your Design Tools, the `<dest>` can be a new folder in HubSpot

As a faster way to develop, the command `gulp push --portal=devportal` will automatically upload your local `src` folder to the `VPB` folder in HubSpot. This also ensures that we only upload to the same folder. This is equivalent to `npx hs upload --portal=<name> src VPB`

 $`gulp push --portal=dev`
 
**IMPORTANT**: To ensure module dependencies are met, always upload to a HubSpot portal with a folder name *VPB*. 

# Getting portal changes to your local

Using the fetch command, run `npx hs fetch --portal=<name> <src> <dest>` to get an entire projects folder tree and files from your HubSpot account into your local environment. The src is the project path in your HubSpot Design Tools, and the dest is the path to the local directory you would like the files to be placed, relative to your current working directory. To fetch the project to your current working directory, you can leave out the `<dest>` argument.

As a faster way to develop, the command `gulp fetch --portal=devportal` will automatically fetch the `VPB` folder in HubSpot to your local. This also ensures that we only upload to the same folder. This is equivalent to `npx hs fetchh --portal=<name> VPB src`
 
 $`gulp fetch --portal=devportal`
 
**IMPORTANT**: This command does not automatically overwrite your local files. If you modified your modules in HubSpot, delete your local module first before fetching.
 

# Compiling SASS manually 
Compiling CSS and outputing to /src/static/css/main.css

 $`gulp build`

# Mirroring and Local CSS and JS injection
Using this command you can mirror a HubSpot page or any page. It will also inject your local main.css and main.js to the mirror page. 

 $`gulp mirror --url=example.com`
 
# Folder Structure and Naming Convention

- All files and folder should be **lowercased** (no camel-cased, no spaces). 
- Dashes `-` is the only special character allowed in files or folder. 
- White spaces are ONLY allowed as a Module Folder Name
```
/app
/app/scripts
/app/styles
/src
/src/modules
/src/templates
/src/static/js
/src/static/css
/src/static/plugins
```

**/app**
- Where we develop SASS, and JS codes.

**/app/scripts**
- Where we develop JS codes. This is where SASS is compiled and put into `/src/static/js/`

**/app/styles**
- Where we develop JS codes. This is where SASS is compiled and put into `/src/static/css/`

**/src**
- the deployable VPB code. This will be the folder that houses all deployable code to HubSpot Portal
 
**/src/modules/**
- Includes all HubSpot module. White spaces for folder names are ONLY allowed in this section. 
 
**/src/static/js**
- For javascript development
 
**/src/static/css**
- For CSS development, This is also where SASS exported CSS will be placed.
  
**/src/static/plugins**
- All downloaded CSS or JS will be added here 
 
 example:
 
```
/src/static/plugins/plyr
/src/static/plugins/plyr/plyr.css
/src/static/plugins/plyr/plyr.js
/src/static/plugins/plyr/images/file.jpg
/src/static/plugins/plugin2/plugin2.css
/src/static/plugins/plugin2/plugin2.js
```


# DEVELOPMENT WORKFLOW

1. Branch the `develop` branch to your feature branch, Each Developer will work on a separate feature.
2. Create a HubSpot developer portal
3. Clone your feature branch to your machine and authorize your newly created developer portal
4. Upload your feature branch to your HubSpot Developer Portal
5. Start Developing

### Creating new fields using the HubSpot Design Manager
1. Create or modify an existing module in HubSpot Design Manager
2. Once fiinshed, Delete the local module you've modified before fetching your HubSpot Developer Portal Modules.
3. Commit your changes.

### Merging your changes to the develop branch
1. Create a new Pull Request in Bitbucket
2. A code review is required before it will be merged to develop

### Deploying to staging
1. Upon Agreement from the wholle team that a Release Candidate is ready. The team lead will tag the current codebase to a RC tag.
2. The team will meet and agree where is the staging portal for a version and the dev lead will then authorize the portal
3. They will need to deploy the current release tag to the staging portal

Note: this will automated in the later part of our process improvement.



## LINKS

* [Netfluence VPB HubSpot App](https://app.hubspot.com/developer/5145327/application/205803)
* [Hubspot Local Module Development Reference](https://designers.hubspot.com/docs/tools/local-module-development)
