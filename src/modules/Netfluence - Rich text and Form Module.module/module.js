/*$(document).ready(function(){
  setTimeout(function(){
    var button = $('.right-form-content .hs-button');
    button.hide();
    
    var buttonLabel = button.val() + '&nbsp; <i class="fas fa-caret-right"></i>';
    var newButton = '<button class="form-submit-btn">' + buttonLabel + '</button>';
    $(newButton).insertAfter($('div.actions'));
    
    $('.form-submit-btn').on('click', function(){
      $('.right-form-content .hs-button').click(); 
    });
  }, 600);
});*/

$(document).ready(function(){
  $( window ).resize(function() {
    setTimeout(function(){
      updateRichTextWidth();
    }, 500);
  });
  
  function updateRichTextWidth() {
    var viewport = $( window ).width();

      if (viewport > 1024) {
        var iframe = $('iframe').length;

        if (iframe) {
          $('.rich-text-content').css('padding-right', '15px');
          $('iframe').attr('width', '600px');
        } 
      } else if (viewport == 1024) {
        var iframe = $('iframe').length;

        if (iframe) {
          $('.rich-text-content').css('padding-right', '0px');
          $('iframe').attr('width', '480px');
        } else {
          $('.rich-text-content').css('padding-right', '10px');
        }
      } else if (viewport == 768) {
        var iframe = $('iframe').length;

        if (iframe) {
          $('.rich-text-content').css('padding-right', '0px');
          $('iframe').attr('width', '720px');
        } 
      } else {
        var iframe = $('iframe').length;

        if (iframe) {
          $('.rich-text-content').css('padding-right', '0px');
          $('iframe').attr('width', '600px');
        }
      }
  }
  
  updateRichTextWidth();
});