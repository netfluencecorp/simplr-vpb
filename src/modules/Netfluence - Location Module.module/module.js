$(document).ready(() => {

  //$(".maps div:not(:first)").hide();
  $(".list:first").removeClass('list').addClass('active');
  $(".addr:first").css('background-color','#ffffff');
  $(".maps div:first").show();
  
  //add cursor pointer
  $(".addr").css('cursor',"pointer");
 
   $(".addr").on('click', function(){
       $('.active').removeClass('active').addClass("list");
       $(".addr").css({'background-color' : ''});

       $(this).closest('div').css({'background-color' : '#ffffff'});
       $(this).siblings('.list').addClass("active");
     
        let map_id = $(this).data('map_id');
        
        $(".maps div").hide();
        $(".maps #location_map_" + map_id).show();
     
      console.log( $(this).data('map_id') );

  });


})